SUMMARY = "Linux kernel driver for IVC InterVM communications"
DESCRIPTION = " \
    This loadable kernel module provides the backend support for \
    inter-domain communications using the IVC InterVM Communications \
    framework. \
    \
    This module provides an interface for userspace processes to \
    communicate securely across VM boundaries. It is recommended \
    that libivc be used as a frontend for using this module. \
"
SECTION = "kernel/modules"

LICENSE  = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"

PV = "0+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src"

inherit module

EXTRA_OEMAKE += " \
    INSTALL_HDR_PATH=${D}${prefix}/src/ivc \
"
MODULES_INSTALL_TARGET += "headers_install"

SYSROOT_DIRS += " \
    ${prefix}/src/ivc \
"

FILES_${PN}-dev += " \
    ${prefix}/src/ivc/include \
"
