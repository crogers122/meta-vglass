SUMMARY = "PV Display Helper for Linux Guests"
DESCRIPTION = " \
    This loadable kernel module provides backend support for Linux \
    PV display drivers intended to interface with the Display Handler. \
    \
    This module abstracts the IVC and Display Handler communications \
    interfaces, simplifying PV display devices and reducing duplication. \
"
SECTION  = "kernel/modules"

LICENSE  = "LGPL-2.1"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fc178bcd425090939a8b634d1d6a9594"

DEPENDS = " \
    kernel-module-ivc \
"

PV = "0+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/pv-display-helper.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit module

EXTRA_OEMAKE += " \
    IVC_INCLUDE_DIR=${RECIPE_SYSROOT}/usr/src/ivc/include \
    INSTALL_HDR_PATH=${D}${prefix}/src/pv_display_helper \
"
MODULES_INSTALL_TARGET += "headers_install"

SYSROOT_DIRS += " \
    ${prefix}/src/pv_display_helper \
"

FILES_${PN}-dev += " \
    ${prefix}/src/pv_display_helper/include \
"
