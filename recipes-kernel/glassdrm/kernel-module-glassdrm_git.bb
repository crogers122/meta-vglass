SUMMARY = "DRM driver for Linux Guests"
DESCRIPTION = "A DRM driver for Linux Guests that provides single or multi-monitor support."
SECTION  = "kernel/modules"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"

DEPENDS = " \
    kernel-module-ivc \
    kernel-module-pv-display-helper \
"
PROVIDES = "glassdrm"

PV = "0+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/glassdrm.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit module

EXTRA_OEMAKE += " \
    IVC_INCLUDE_DIR=${RECIPE_SYSROOT}/usr/src/ivc/include \
    DDH_INCLUDE_DIR=${RECIPE_SYSROOT}/usr/src/pv_display_helper/include \
"

do_install_append() {
    install -d ${D}${sysconfdir}/udev/rules.d
    install -d ${D}${datadir}/X11/xorg.conf.d
    install -d ${D}${datadir}/svdrm

    install -m 644 ${S}/10-svdrm.rules ${D}${sysconfdir}/udev/rules.d
    install -m 644 ${S}/60-svdrm.conf ${D}${datadir}/X11/xorg.conf.d
    install -m 755 ${S}/display_hotplug.sh ${D}${datadir}/svdrm
}

RPROVIDES_${PN} = "svdrm"

FILES_${PN} += " \
    ${sysconfdir}/udev/rules.d/10-svdrm.rules \
    ${datadir}/X11/xorg.conf.d/60-svdrm.conf \
    ${datadir}/svdrm/display_hotplug.sh \
"
