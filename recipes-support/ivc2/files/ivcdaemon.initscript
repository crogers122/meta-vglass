#!/bin/sh
#
# Copyright (c) 2020 Assured Information Security, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

### BEGIN INIT INFO
# Provides:          ivcdaemon
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: ivc daemon
### END INIT INFO

. /etc/default/rcS
. /etc/init.d/functions

DESC="ivcdaemon"
PROG="/usr/bin/ivcdaemon"
PIDFILE="/var/run/ivcdaemon.pid"

[ -f "$PROG" ] || exit 0

do_start() {
    start-stop-daemon --start --quiet --oknodo --background \
        --pidfile "${PIDFILE}" --make-pidfile \
        --exec "${PROG}"
}

do_stop() {
    start-stop-daemon --stop --oknodo --pidfile "${PIDFILE}"
    rm -f "${PIDFILE}"
}

case "$1" in
    start)
        begin "Starting ${DESC}"
        do_start
        ;;
    stop)
        begin "Stopping ${DESC}"
        do_stop
        ;;
    restart|reload)
        begin "Restarting ${DESC}"
        do_stop
        do_start
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|reload}"
        exit 1
        ;;
esac
