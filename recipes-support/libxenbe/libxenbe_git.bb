#License information for the recipe.
LIC_FILES_CHKSUM = "file://LICENSE;md5=a23a74b3f4caf9616230789d94217acb"
LICENSE = "GPL-2.0"

SRCREV = "d42279073e2a3303ae96f9b098a0bfb55ee13d87"
SRC_URI = "git://github.com/xen-troops/libxenbe.git;protocol=git"
PV = "0+git${SRCPV}"
S = "${WORKDIR}/git/"

PACKAGECONFIG ??= ""
PACKAGECONFIG[debug] += "-DCMAKE_BUILD_TYPE=Debug,-DCMAKE_BUILD_TYPE=Release"

inherit cmake

DEPENDS = "xen-tools"

FILES_${PN} += " \
    ${libdir}/libxenbe.so \
"
FILES_SOLIBSDEV = ""
