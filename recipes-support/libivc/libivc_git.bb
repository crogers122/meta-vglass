# Copyright (C) 2015 Assured Information Security, Inc.
# Recipe released under the MIT license (see COPYING.MIT for the terms)

SUMMARY = "Userspace library for accessing the InterVM Communication modules"
DESCRIPTION = " \
    This library provides a set of userspace functions for interacting with \
    the IVC (InterVM Communication) kernel modules. These functions can be used \
    to gain simple file and shared-memory communications channel between VMs \
    running on the Xen hypervisor. \
"
AUTHOR = "\
    Kyle J. Temkin <temkink@ainfosec.com> \
    Brendan Kerrigan <kerriganb@ainfosec.com> \
"
LIC_FILES_CHKSUM = "file://../../LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"
LICENSE = "GPLv2"

PV     = "1+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

PROVIDES = "virtual/libivc"

S = "${WORKDIR}/git/src/us"

PACKAGECONFIG ??= ""
PACKAGECONFIG[debug] = "-DCMAKE_BUILD_TYPE=Debug,-DCMAKE_BUILD_TYPE=Release"

inherit cmake

# debian.bbclass will rename libivc[12]-* to libivc-*.
# This ends up raising Exception: FileNotFoundError when do_rootfs tries to
# create_packages_dir.
AUTO_LIBNAME_PKGS = ""

RDEPENDS_${PN} += "kernel-module-ivc"
RCONFLICTS_${PN} = "libivc2"
RCONFLICTS_${PN}-dbg = "libivc2-dbg"
RCONFLICTS_${PN}-dev = "libivc2-dev"
