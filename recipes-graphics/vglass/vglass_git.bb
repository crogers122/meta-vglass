DESCRIPTION = "Graphics Stack"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"
DEPENDS = " \
    dbus \
    libdrm \
    libinput \
    virtual/libivc \
    libpvglass \
    libxenbackend \
    qtbase \
"

PV = "0+git${SRCPV}"

SRC_URI = " \
    git://gitlab.com/vglass/glass.git;protocol=https;branch=master \
    file://discrete_gfx.conf \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

PACKAGECONFIG ??= ""
PACKAGECONFIG[vnc] += "CONFIG+=vnc_renderer,,libvncserver"
PACKAGECONFIG[debug] += "CONFIG+=debug DEFINES+=DEBUG,CONFIG+=release DEFINES+=NDEBUG"
PACKAGECONFIG[profile] += "CONFIG+=profiler,,gperftools"

require recipes-qt/qt5/qt5.inc
inherit update-rc.d pkgconfig

EXTRA_QMAKEVARS_PRE += "${PACKAGECONFIG_CONFARGS}"

do_install_append() {
    if ! ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
        install -d ${D}/etc/init.d
        install -m 755 ${S}/init.d/vglass ${D}${sysconfdir}/init.d/vglass
    fi

    # Modprobe rules that prevent loading of discrete graphics modules from
    # outside of the display handler.
    install -d ${D}/etc/modprobe.d
    install -m 755 ${WORKDIR}/discrete_gfx.conf ${D}/etc/modprobe.d/discrete_gfx.conf

    install -m 755 ${S}/bin/discrete_gfx_modprobe.sh ${D}${bindir}/discrete_gfx_modprobe.sh

}

INITSCRIPT_NAME = "vglass"
INITSCRIPT_PARAMS = "start 98 5 . stop 2 0 1 2 3 4 6 ."

# Not using -tools pkg-split.
PACKAGES_remove += "${PN}-tools"
FILES_${PN} += " \
    ${OE_QMAKE_PATH_BINS} \
    ${sysconfdir}/mosaic \
"
FILES_${PN}-dev += " \
    ${includedir}/common \
    ${includedir}/toolstack \
"

# libpvbackendhelper should already be in there, something's wrong with that
# package.
RDEPENDS_${PN} += " \
    bash \
    ivc2 \
    libpvbackendhelper \
    xenclient-keyboard-list \
    ttf-dejavu-sans \
"

